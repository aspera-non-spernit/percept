pub mod generate {
    use rand::{ Rng, seq::SliceRandom };
    pub const D: [usize; 12] = [0, 1, 2, 5, 8, 10, 13, 15, 18, 20, 21, 22];
    pub const A: [usize; 14] = [1, 2, 3, 5, 9 , 10 , 11, 12, 13, 14, 15, 19, 20, 24];
    pub const N: [usize; 14] = [0, 4, 5, 6, 9, 10, 12, 14, 15, 17, 19, 20, 23, 24];
    pub const I: [usize; 5] = [2, 7, 12, 17, 22];
    pub const E: [usize; 16] = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 15, 20, 21, 22, 23, 24];
    pub const L: [usize; 9] = [0, 5, 10, 15, 20, 21, 22, 23, 24];
    pub const X: [usize; 9] = [0, 4, 6, 8, 12, 16, 18, 20, 24];
    pub const O: [usize; 12] = [1, 2, 3, 5, 9, 10, 14, 15, 19, 21, 22, 23];
    
    pub const CLASSES: [char; 6] = ['D', 'A', 'N', 'I', 'E', 'L'];
    
    pub fn generate_letter(pixels: &[usize], model: bool) -> Vec<f64> {
        let mut model_high = 0.8;
        let mut model_low = 0.2;
    
        if model == true { model_high = 1.0 ; model_low = 0.0; }
    
        let mut rng = rand::thread_rng();
        (0..25)
            .map(|i| {
                if  pixels.contains(&i) {
                    rng.gen_range(model_high..=1.0)
                } else {
                    rng.gen_range(0.0..=model_low)
                }
            }).collect::<Vec<f64>>()
    }

    pub fn training_set(model: bool) -> Vec< ( Vec<f64>, Vec<f64>) > {
        let mut inputs = Vec::new();

        for _ in 0..1 { 
            inputs.push((generate_letter(&D, model), vec![1.0, 0.0, 0.0, 0.0, 0.0, 0.0]));
            inputs.push((generate_letter(&A, model), vec![0.0, 1.0, 0.0, 0.0, 0.0, 0.0]));
            inputs.push((generate_letter(&N, model), vec![0.0, 0.0, 1.0, 0.0, 0.0, 0.0]));
            inputs.push((generate_letter(&I, model), vec![0.0, 0.0, 0.0, 1.0, 0.0, 0.0]));
            inputs.push((generate_letter(&E, model), vec![0.0, 0.0, 0.0, 0.0, 1.0, 0.0]));
            inputs.push((generate_letter(&L, model), vec![0.0, 0.0, 0.0, 0.0, 0.0, 1.0]));
        }
        inputs
    }
}

fn main() {
     // generating and shuffling a training set
     let num_training_sets = 17; // A set of all letters D, A, N, I, E, L (17 means 17 D's 17 A's..)
     let mut rng = rand::thread_rng();
     let mut training_set: Vec<(Vec<f64>, Vec<f64>)> = (0..num_training_sets)
         .flat_map(|_| training_set(false)
         .into_iter())
         .collect::<Vec<_>>();
     
     training_set.as_mut_slice().shuffle(&mut rng);
 
     dbg!(&training_set);
     
     // input and one hidden layer using the relu activation
     let input_topology = [training_set[0].0.len()]; // 6, training_set[0].1.len()
     dbg!(&input_topology);
 
     // the output layer
     let output_topology = [6];
     dbg!(&output_topology);
  
     let mut input_net = Network::new(&input_topology);
     let mut output_net = Network::new(&output_topology);
     dbg!(&input_net);
     dbg!(&output_net);
  
     // training config
     let act_fn = tanh;
  
     let mut max_iterations = 70; // 50 - 100
     let conf_1 = Config {learning_rate: 0.001, threshold: 0.01, max_iterations, act_fn: act_fn };
     dbg!(&conf_1);
   
     for _ in 1..=conf_1.max_iterations {
         for (input, target) in &training_set {
             input_net.train(&conf_1, input, target);
         }
     }
 /*
     // changing iterations and activation function for output layers/ network
     max_iterations = 50; // 20 - 30 less the net1
     let act_fn = sigmoid;
 
     let conf_2 = Config {learning_rate: 0.001, threshold: 0.01, max_iterations, act_fn: act_fn };
 
     // Forward pass through the input network to get the output for the second network
     let input_to_output_layer = input_net.layer_weights.last().cloned().unwrap();
 
     for _ in 1..=conf_2.max_iterations {
         for (_, target) in &training_set {
             output_net.clip_gradients(conf_2.learning_rate);
             output_net.train(&conf_2, &input_to_output_layer, target);
         }
     }
     
     // training
     let letter = ("A", generate_letter(&generate::A, false )) ;
     let mut test_output = test(&letter.1, input_net.layer_weights.clone(), conf_1.act_fn);
    //  test_output = test(&letter.1, output_net.layer_weights.clone(), conf_2.act_fn);
     dbg!(&test_output);
     
     let predicted_class = test_output
     .iter()
     .enumerate()
     .min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap()) // min_by sigmoid, 
     .map(|(idx, _)| idx);
 
     // If the predicted_class is Some(index), map it to the corresponding class; otherwise, handle the case when it's None.
     // may not work with all activation functions.
     let predicted_class_label = predicted_class.map_or_else(|| 'U', |idx| generate::CLASSES.get(idx).cloned().unwrap_or('U'));
     println!("Should be: {:?}, predicted Class: {:?}", letter.0, test_output);
     println!("Should be: {:?}, predicted Class: {:?}", letter.0, predicted_class_label);
     
 */
}