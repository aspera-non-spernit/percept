
fn main() {
    let x = Signal {
        values : [
            [1.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [1.0, 0.0, 1.0]
        ]
    };
    
    let noisy_x = x.to_noisy();
    let weights = weight_matrix();
    multiply_matrices(noisy_x.values.iter().flatten().copied().collect(), weights);
}