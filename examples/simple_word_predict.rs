use std::collections::{HashMap, HashSet};
use rand::Rng;
use rand::seq::SliceRandom;


const EN_ENDINGS: [&str; 3] = ["s", "ing", "ed"];
const DE_ENDINGS: [&str; 8] = ["e", "en", "er", "es", "n", "s", "st", "t"];
const EN_STOP_WORDS: [&str; 2] = ["is", "a"];
const DE_STOP_WORDS: [&str; 21] = ["aber", "alle", "allem", "allen", "aller", "alles", "als", "also", "am", "an", "ander", "andere", "anderem", "anderen", "anderer", "anderes", "anderm", "andern", "anderr", "anders", "auch"
];
const SPECIAL_CHARACTERS: &str = r#".!?,;'"()[]{}<>:;/"#;

fn clean(input_string: &str, symbols_to_remove: &str) -> String {
    // Define a closure to check if a character is a symbol to remove
    let is_symbol = |c: char| symbols_to_remove.contains(c);

    // Filter out characters that are symbols to remove
    let cleaned_string: String = input_string.chars()
        .filter(|&c| !is_symbol(c))
        .collect();

    cleaned_string
}

fn tokenize(input_string: &str, is_clean: bool, token_delimiters: &str) -> Vec<String> {
    // Clean the input string if 'should_clean' is true
    let cleaned_string = if !is_clean {
        clean(input_string, "")
    } else {
        input_string.to_string()
    };

    // Tokenize the cleaned string using the specified delimiters
    let tokens: Vec<String> = cleaned_string.split(token_delimiters)
        .filter(|&token| !token.is_empty())
        .map(|token| token.to_string())
        .collect();

    tokens
}

fn rm_stopwords(tokens: Vec<String>, stop_words: &[&str]) -> Vec<String> {
    // Convert the stop words to a HashSet for efficient lookup
    let stop_words_set: std::collections::HashSet<&str> = stop_words.iter().cloned().collect();

    // Filter out tokens that are not stop words
    let filtered_tokens: Vec<String> = tokens.into_iter()
        .filter(|token| !stop_words_set.contains(token.as_str()))
        .collect();

    filtered_tokens
}

enum EmbeddingAlgorithm {
    GloVe,
    FastText,
    Word2Vec
}

fn embeddings(tokens: &[String], algorithm: EmbeddingAlgorithm) -> HashMap<String, Vec<f64>> {
    match algorithm {
        EmbeddingAlgorithm::GloVe => glove(tokens),
        EmbeddingAlgorithm::FastText => fasttext(tokens),
        EmbeddingAlgorithm::Word2Vec => word2vec(tokens),
    }
}

fn glove(tokens: &[String]) -> HashMap<String, Vec<f64>> {
    // Placeholder implementation for GloVe embeddings
    println!("Calculating GloVe embeddings...");
    let mut word_embeddings: HashMap<String, Vec<f64>> = HashMap::new();
    
    // Generate random vectors for each word
    let mut rng = rand::thread_rng();
    for token in tokens {
        let embedding: Vec<f64> = (0..100) // Assuming 100-dimensional embeddings for demonstration
            .map(|_| rng.gen_range(-1.0..1.0)) // Random values between -1.0 and 1.0
            .collect();
        word_embeddings.insert(token.clone(), embedding);
    }
    
    word_embeddings
}

fn fasttext(tokens: &[String]) -> HashMap<String, Vec<f64>> {
    // Placeholder implementation for FastText embeddings
    println!("Calculating FastText embeddings...");
    let mut word_embeddings: HashMap<String, Vec<f64>> = HashMap::new();

    // Generate random vectors for each word
    let mut rng = rand::thread_rng();
    for token in tokens {
        let embedding: Vec<f64> = (0..100) // Assuming 100-dimensional embeddings for demonstration
            .map(|_| rng.gen_range(-1.0..1.0)) // Random values between -1.0 and 1.0
            .collect();
        word_embeddings.insert(token.clone(), embedding);
    }

    word_embeddings
}
fn word2vec(tokens: &[String]) -> HashMap<String, Vec<f64>> {
    let mut word_embeddings: HashMap<String, Vec<f64>> = HashMap::new();
    let mut word_counts: HashMap<String, usize> = HashMap::new();

    // Calculate word counts
    for token in tokens {
        let count = word_counts.entry(token.clone()).or_insert(0);
        *count += 1;
    }

    // Calculate embeddings
    for (word, count) in word_counts.iter() {
        let mut embedding = vec![0.0; 100]; // Assuming 100-dimensional embeddings for simplicity
        let norm_factor = *count as f64;

        for (i, val) in embedding.iter_mut().enumerate() {
            *val = (word.len() as f64 * 0.1) * (i as f64 + 1.0) / norm_factor; // Simple calculation for demonstration
        }

        word_embeddings.insert(word.clone(), embedding);
    }

    word_embeddings
}

fn padding(tokens: Vec<String>, max_length: usize) -> Vec<String> {
    let mut padded_tokens = tokens;

    // If the tokens length is less than max_length, pad with an empty string
    while padded_tokens.len() < max_length {
        padded_tokens.push(String::new());
    }

    // If the tokens length is greater than max_length, truncate
    if padded_tokens.len() > max_length {
        padded_tokens.truncate(max_length);
    }

    padded_tokens
}

fn encode(tokens: &[String]) -> Vec<Vec<u8>> {
    // Create a hashmap to store the index of each unique token
    let mut token_index_map: HashMap<&String, usize> = HashMap::new();
    let mut index = 0;

    // Assign an index to each unique token
    for token in tokens {
        if !token_index_map.contains_key(token) {
            token_index_map.insert(token, index);
            index += 1;
        }
    }

    // Create a vector to store the encoded sequences
    let mut encoded_sequences: Vec<Vec<u8>> = Vec::new();

    // Encode each token sequence
    for token in tokens {
        // Create a binary vector with zeros
        let mut encoded_sequence = vec![0_u8; token_index_map.len()];

        // Set the corresponding index to 1
        if let Some(&token_index) = token_index_map.get(token) {
            encoded_sequence[token_index] = 1;
        }

        // Add the encoded sequence to the result vector
        encoded_sequences.push(encoded_sequence);
    }

    encoded_sequences
}

fn split_dataset(dataset: Vec<String>, train_ratio: f64, val_ratio: f64, test_ratio: f64) -> (Vec<String>, Vec<String>, Vec<String>) {
    assert_eq!(train_ratio + val_ratio + test_ratio, 1.0, "Ratios must sum up to 1.0");

    let total_samples = dataset.len();
    let train_size = (train_ratio * total_samples as f64).round() as usize;
    let val_size = (val_ratio * total_samples as f64).round() as usize;
    let test_size = (test_ratio * total_samples as f64).round() as usize;

    // Shuffle the dataset
    let mut rng = rand::thread_rng();
    let mut shuffled_dataset = dataset.clone();
    shuffled_dataset.shuffle(&mut rng);

    // Split the dataset into training, validation, and test sets
    let train_set = shuffled_dataset[..train_size].to_vec();
    let val_set = shuffled_dataset[train_size..(train_size + val_size)].to_vec();
    let test_set = shuffled_dataset[(train_size + val_size)..].to_vec();

    (train_set, val_set, test_set)
}
fn ngrams(input: &[String], n: usize) -> Vec<Vec<String>> {
    let mut result = Vec::new();
    
    for i in 0..(input.len() - n + 1) {
        let mut n_gram = Vec::new();
        for j in 0..n {
            n_gram.push(input[i + j].clone());
        }
        result.push(n_gram);
    }
    
    result
}

// really bad
fn stemming(token: &str, endings: &[&str]) -> String {
    // Check if the token ends with any of the specified endings
    for &ending in endings {
        if token.ends_with(ending) {
            // Remove the ending and return the stemmed token
            return token[..token.len() - ending.len()].to_string();
        }
    }
    // If no ending is found, return the original token
    token.to_string()
}

fn probabilities(n_grams: &[Vec<String>]) -> HashMap<Vec<String>, HashMap<String, f64>> {
    let mut transitions_count: HashMap<Vec<String>, HashMap<String, usize>> = HashMap::new();
    
    // Count occurrences of transitions
    for gram in n_grams.windows(2) {
        let current_state = &gram[0];
        let next_state = &gram[1][0];
        
        let entry = transitions_count.entry(current_state.clone()).or_insert_with(HashMap::new);
        *entry.entry(next_state.clone()).or_insert(0) += 1;
    }
    
    // Normalize counts to obtain probabilities
    let mut probabilities: HashMap<Vec<String>, HashMap<String, f64>> = HashMap::new();
    for (current_state, transitions) in transitions_count.iter() {
        let total_count: usize = transitions.values().sum();
        let mut probabilities_map = HashMap::new();
        
        for (next_state, &count) in transitions.iter() {
            let probability = count as f64 / total_count as f64;
            probabilities_map.insert(next_state.clone(), probability);
        }
        
        probabilities.insert(current_state.clone(), probabilities_map);
    }
    
    probabilities
}

fn predict(previous_words: &[String], probabilities: &HashMap<Vec<String>, HashMap<String, f64>>) -> Option<String> {
    for n in (1..=previous_words.len()).rev() {
        let prefix = &previous_words[(previous_words.len() - n)..];
       // println!("Prefix: {:?}", prefix);
        if let Some(next_word_probabilities) = probabilities.get(prefix) {
           // println!("Next word probabilities: {:?}", next_word_probabilities);
            let mut rng = rand::thread_rng();
            let random_value: f64 = rng.gen(); // Generate a random value between 0 and 1
          //  println!("Random value: {}", random_value);
            let mut cumulative_probability = 0.0;

            for (next_word, &probability) in next_word_probabilities.iter() {
                cumulative_probability += probability;
         //       println!("Cumulative probability: {}", cumulative_probability);
                if cumulative_probability >= random_value {
                    return Some(next_word.clone());
                }
            }
        }
    }

    None
}

fn main() {
    let corpus = vec![
        "Als der russische Staatschef kurz nach der Invasion der Ukraine persönlich mit Sanktionen belegt wurde, hielt man das zunächst für einen eher symbolischen Akt - denn auf dem Papier besitzt Wladimir Putin gar kein nennenswertes Vermögen. Dennoch gibt es einen luxuriösen Palast am Schwarzen Meer, der ihm in den Medien zugerechnet wird, und kurz vor Kriegsausbruch fuhr auch eine Jacht, die ihm angeblich gehören soll, schnell von Europa nach Russland zurück. ",
        "Mehrere Hundert Milliarden Dollar russischer Vermögenswerte liegen eingefroren auf Auslandskonten - zum großen Teil in der Europäischen Union. Nun hat die EU-Kommissionspräsidentin Ursula von der Leyen dafür geworben, mit Gewinnen aus diesen Werten Waffen für die Ukraine zu finanzieren. ",
        "Gestern wurde die Ex-RAF-Terroristin Klette in Berlin festgenommen - und noch eine weitere Person gefasst. Doch laut LKA handelt es sich dabei nicht um einen der weiteren gesuchten Ex-Terroristen.",
        "Nach der zweiten Festnahme im Fall der Ex-RAF-Terroristin Daniela Klette ist der Verdächtige freigelassen worden. Wie das Landeskriminalamt in Hannover mitteilte, ist er zweifelsfrei weder Burkhard Garweg noch Ernst-Volker Staub, die beiden anderen noch flüchtigen Straftäter aus diesem Zusammenhang.",
        "Kurz nach der gestrigen Festnahme Klettes hatten die Fahnder in Berlin laut LKA eine weitere Person festgenommen. Es handele sich um einen Mann im \"gesuchten Alterssegment\", sagte LKA-Präsident Friedo de Vries. Die Staatsanwaltschaft Verden und das Landeskriminalamt fahnden seit Jahrzehnten nach den früheren RAF-Terroristen der sogenannten dritten RAF-Generation.",
        "Vor kurzem war am Wuppertaler Hauptbahnhof ein Mann aus einem Zug geholt und festgenommen worden, weil ein Augenzeuge ihn für Staub gehalten hatte. Dieser Verdacht bestätigte sich aber nicht. ",
        "Auch Großbritanniens Premierminister Rishi Sunak brachte einen ähnlichen Vorschlag. In einem Gastbeitrag für die \"Sunday Times\" sprach er sich vor wenigen Tagen für einen energischeren Umgang mit eingefrorenem russischen Kapital aus. \"Das fängt damit an, dass die Milliarden an Zinsen, die für diese Vermögen anfallen, an die Ukraine geschickt werden\", schrieb der konservative Politiker. In einem zweiten Schritt müssten dann im Rahmen der führenden westlichen Industrienationen G7 legale Wege gefunden werden, um die Vermögen selbst zu beschlagnahmen und sie an die Ukraine weiterzureichen. ",
        "Klette, Staub und Garweg werden unter anderem versuchter Mord und eine Serie schwerer Raubüberfälle vorgeworfen. Die drei waren schon in den 1990er-Jahren untergetaucht. DNA-Spuren brachten die Ermittler darauf, dass sie für Raubüberfälle auf Geldtransporte und Supermärkte im Zeitraum zwischen 1999 und 2016 verantwortlich sein könnten. ",
        "Tatorte waren unter anderem Osnabrück, Wolfsburg und Stuhr in Niedersachsen sowie Hagen und Bochum-Wattenscheid in Nordrhein-Westfalen.",
        "Forderungen, die eingefrorenen Vermögenswerte auf russischen Auslandskonten an die Ukraine weiterzuleiten, wurden zuletzt immer lauter. So sagte US-Finanzministerin Janet Yellen im Vorfeld des G20-Finanzministertreffens im brasilianischen São Paulo, es sei dringend notwendig einen Weg zu finden, den Wert dieser stillgelegten Vermögenswerte freizusetzen, \"um den anhaltenden Widerstand und den langfristigen Wiederaufbau der Ukraine zu unterstützen.\"",
        "Es ist an der Zeit, ein Gespräch darüber zu beginnen, die unerwarteten Gewinne aus den eingefrorenen russischen Vermögenswerten zu nutzen, um gemeinsam militärische Ausrüstung für die Ukraine zu kaufen\", sagte sie vor dem Europäischen Parlament. \"Es könnte kein stärkeres Symbol und keine bessere Verwendung für dieses Geld geben, als die Ukraine und ganz Europa zu einem sichereren Ort zum Leben zu machen.",
        "Die Staatsanwaltschaft geht davon aus, dass die Überfälle nicht politisch motiviert waren. Die Beschuldigten sollen die Taten begangen haben, um an Geld zu kommen. ",
        "Gegen Klette wurde Haftbefehl erlassen. Nach übereinstimmenden Medienberichten ist sie aktuell in Untersuchungshaft in der JVA Vechta. Die 65-Jährige ist demnach vom Amtsgericht Verden in das dortige Frauengefängnis gebracht worden. Weder die zuständige Staatsanwaltschaft in Verden noch das Justizministerium in Hannover wollten sich auf Anfrage dazu äußern. ",
        "Die Kommissionschefin wiederholte ihr Versprechen, die Ukraine so lange wie nötig zu unterstützen. \"Wir dürfen Russland nicht gewinnen lassen\", betonte sie. ",
        "Die linksextremistische Vereinigung Rote Armee Fraktion (RAF) galt in der Bundesrepublik über Jahrzehnte als Inbegriff des Terrorismus. Insgesamt ermordete die RAF mehr als 30 Menschen, mehr als 200 wurden verletzt. Die RAF löste sich 1998 auf. ",
        "Die EU-Länder hatten sich Ende Januar bereits darauf geeinigt, Zinsgewinne aus den eingefrorenen russischen Vermögenswerten für die Ukraine beiseite zu legen. Seit Beginn des russischen Angriffskriegs hat die Europäische Union etwa 200 Milliarden Euro an russischen Vermögenswerten eingefroren. Wegen hoher juristischer Hürden in Deutschland und anderen Ländern können sie aber nicht einfach beschlagnahmt werden. ",
        "Staub, Klette und Garweg werden der sogenannten dritten RAF-Generation zugeordnet. Vertreter der Generation sollen den damaligen Chef der Deutschen Bank, Alfred Herrhausen, und den Treuhand-Chef, Detlev Karsten Rohwedder, umgebracht haben. "
    ];
    // println!("Input text (example): {}", corpus[0]);
    // Step 1: Consolidate all tokens into one vector
    let tokens: Vec<String> = corpus.iter().flat_map(|s| {
        // Step 2: Clean the text
        //let cleaned_text = clean(s, SPECIAL_CHARACTERS);
        // println!("Cleaned text: {}", cleaned_text);
        // Step 3: Tokenize the cleaned text
        tokenize(&s, true, " ")

       // tokenize(&cleaned_text, true, " ")
    }).collect();
    
    // Step 3: Remove stop words
    let filtered_tokens = rm_stopwords(tokens, &DE_STOP_WORDS);

    // Step 4: Padding
    let padded_tokens = padding(filtered_tokens.clone(), 727); // Cloning filtered_tokens

    // Step 5: Encode tokens
    let encoded_sequences = encode(&padded_tokens);

    // Step 6: Split the dataset
    let (train_set, val_set, test_set) = split_dataset(padded_tokens.clone(), 1.0, 0.0, 0.0);

    // ngrams
    let ngrams = ngrams(&padded_tokens, 1);
    // Display the results
  /*
    println!("Filtered tokens({:?}): {:?}", filtered_tokens.len(), filtered_tokens);
    println!("Padded tokens({:?}): {:?}", padded_tokens.len(), padded_tokens);
    println!("Encoded sequences: {:?}", encoded_sequences);

    println!("Train set: {:?}", train_set);
    println!("Validation set: {:?}", val_set);
    println!("Test set: {:?}", test_set);
    */
  //  println!("nGrams: {:#?}", ngrams);
    let probabilities = probabilities(&ngrams);
  //  println!("probabilities: {:?}", probabilities);
    let mut previous_words = vec!["Die".to_string()];
    print!("{:?} ", previous_words);
    for _ in 0..500 {
        if let Some(next_word) = predict(&previous_words, &probabilities) {
            print!("{} ", next_word);
            previous_words.push(next_word);
            previous_words.remove(0); // Remove the first word to keep the window size consistent
        } else {
            println!("\nNo prediction available for the given sequence of words.");
            break;
        }
    }
}

