# PERCEPT

**percept** is a small artificial neural network that classifies simple input vectors using a single (output layer) consisting of an arbitrary number of input and output neurons (here called perceptron), hence the name of the program.

The example executable in **percept**

- generates the letters X and O
- as a flattened 5x5 pixel vector of a 1-channel image 
- with either perfect pixel intensities, values 1.0 and 0.0 or 
- in a more blurry fashion, where active pixel intensities range from 0.7 to 1.0 and inactive pixels range from 0.0 to 0.2

# ChatGPT 

- Conversation: https://chat.openai.com/share/cc6657b4-8239-4145-90a0-cfcbe45814ae
- Gitlab Project: https://gitlab.com/aspera-non-spernit/percept/_chatGPT

I intended to code an artificial neural network for unsupervised clustering of faux images i would generate in code similarly to MNIST or alike. However ChatGPT diverted quickly and suggested code leading to a classifier that needed a target set too. 

Half-way through I was stuck in errors, so I told ChatGPT to forget everything and to start over. (Search for the term 'start over' if you want to read the second part of the conversation).

So after we took a deep breath and I deleted faulty code, I provided ChatGPT with the code I had left, asked ChatGPT if it understands the intention of the code and asked to implement missing code step-by-step. I was guiding ChatGPT through everything and made ChatGPT correct its own errors which it did quickly.

ChatGPT makes lots of errors, but it can fix them, if you tell what it is. It's even possible to post the compiler errors. rust is providing incredible compiler messages.

I tested the network after it was completed. The test set consisted of four examples, two letters X and two O letters. **percept** generates the character vectors (flattened 5x5) in with either perfect pixel intensities (values 0.0 and 1.0) or in a more blurry fashion, where 'active' pixels have values from 0.7-1.0 and inactive pixels have values from 0.0-0.2.

It's a simple program, but it's quite possible that you can show ChatGPT this version of the program and work from there to make it better.

Although I have some CS background, I have only programmed two small 'intelligent' programs. One that predicts the winner of a football match called [guru](https://gitlab.com/aspera-non-spernit/guru) given only previous results and info on guest or home player (+80% success rate, 3rd division NISA US soccer league) and a genetic algorithm that solves the [travelling sales person problem](https://gitlab.com/aspera-non-spernit/tsp) I once programmed for university, to be used for high school teachers to teach genetic algorithms (grades 11+).

I have to admit, that despite the many errors ChatGPT may already be the better programmer. Due to my background and knowledge of ANN i was able to guide ChatGPT through the project. 

I think including the first attempt that was mostly abandoned it took only 2 hours to code this little program. 

I am impressed.

**Note:**

The [lib-folder](https://gitlab.com/aspera-non-spernit/percept/-/tree/dev?ref_type=heads) in the root of this project is not related to the ChatGPT experiment.

For the original version of ```percept``` written mostly by ChatGPT, please check out commit [```cf59d913```](https://gitlab.com/aspera-non-spernit/percept/-/commit/cf59d913866ec3718bc8dc61745a16fb6bb0a32a) and [```1aeb2d1d```](https://gitlab.com/aspera-non-spernit/percept/-/commit/cf59d913866ec3718bc8dc61745a16fb6bb0a32a). 

