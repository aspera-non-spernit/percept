pub mod error;

use rand::Rng;
use std::f64::consts::E;

trait Activation { fn activate(&self, weighted_sum: f64) -> f64; }

pub trait Train { fn train(&mut self, training_set: &TrainingSet, learning_rate: f64, epochs: usize); }

#[derive(Clone, Debug)] 
pub enum ActivationFunction { Sigmoid, Step, Tanh }

#[derive(Debug)]
pub struct Layer { pub perceptrons: Vec<Perceptron>, }

#[derive(Debug)]
pub struct Perceptron {
    weights: Vec<f64>,
    bias: f64,
    activation: ActivationFunction,
}

#[derive(Debug)]
pub struct TrainingSet {
    pub inputs: Vec<Vec<f64>>,
    pub targets: Vec<Vec<f64>>,
}

impl Activation for ActivationFunction {
    fn activate(&self, weighted_sum: f64) -> f64 {
        match self {
            ActivationFunction::Step => {
                if weighted_sum >= 0.0 {
                    1.0
                } else {
                    0.0
                }
            },
            ActivationFunction::Tanh => {
                (E.powf(2.0 * weighted_sum) - 1.0) / (E.powf(2.0 * weighted_sum) + 1.0)
            },
            ActivationFunction::Sigmoid => {
                1.0 / (1.0 + (- weighted_sum).exp())
            }
        }    
    }
}

impl Train for Perceptron {
    fn train(&mut self, training_set: &TrainingSet, learning_rate: f64, epochs: usize) {
        assert_eq!(
            training_set.inputs.len(),
            training_set.targets.len(),
            "Input and target length mismatch"
        );

        for _ in 0..epochs {
            for (input, target) in training_set.inputs.iter().zip(&training_set.targets) {
                let prediction = self.activate(input);
                let error = error::compute_error(self, training_set);

                // Update weights based on the error and learning rate
                for (weight, &input_val) in self.weights.iter_mut().zip(input) {
                    *weight += learning_rate * error * input_val;
                }

                // Update bias based on the error and learning rate
                self.bias += learning_rate * error;
            }
        }
    }
}

impl Perceptron {
    pub fn new(weights: Vec<f64>, activation: ActivationFunction) -> Perceptron {
        Perceptron {
            weights,
            bias: 1.0,
            activation,
        }
    }

    pub fn activate(&self, inputs: &[f64]) -> f64 {
        let weighted_sum: f64 = inputs.iter().zip(&self.weights).map(|(&x, &w)| x * w).sum();
        let weighted_sum_with_bias = weighted_sum + self.bias;
        self.activation.activate(weighted_sum_with_bias)
    }
}

impl Default for Perceptron {
    fn default() -> Perceptron {
        let mut rng = rand::thread_rng();

        // Generate random weights between -1.0 and 1.0
        let weights: Vec<f64> = (0..25).map(|_| rng.gen_range(-1.0..=1.0)).collect();

        Perceptron {
            weights,
            bias: 1.0,
            activation: ActivationFunction::Sigmoid,
        }
    }
}

impl Layer {
    pub fn new(num_inputs: usize, num_perceptrons: usize, activation: ActivationFunction) -> Layer {
        let mut rng = rand::thread_rng();

        let perceptrons = (0..num_inputs)
            .map(|_| Perceptron::new((0..num_perceptrons).map(|_| rng.gen_range(-1.0..=1.0)).collect(), activation.clone()))
            .collect();

        Layer { perceptrons }
    }
}

impl Train for Layer {
    fn train(&mut self, training_set: &TrainingSet, learning_rate: f64, epochs: usize) {
        assert_eq!(
            training_set.inputs.len(),
            training_set.targets.len(),
            "Input and target length mismatch"
        );

        let num_perceptrons = self.perceptrons.len();
        let num_inputs = self.perceptrons[0].weights.len();

        for _ in 0..epochs {
            for (input, target) in training_set.inputs.iter().zip(&training_set.targets) {
                let predictions: Vec<f64> = self.perceptrons.iter().map(|p| p.activate(input)).collect();
                let errors: Vec<f64> = predictions.iter().zip(target).map(|(&prediction, &t)| t - prediction).collect();

                // Update weights based on the errors and learning rate
                for i in 0..num_perceptrons {
                    for j in 0..num_inputs {
                        self.perceptrons[i].weights[j] += learning_rate * errors[i] * input[j];
                    }

                    self.perceptrons[i].bias += learning_rate * errors[i];
                }
            }
        }
    }
}
