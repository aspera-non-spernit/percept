use crate::{ Perceptron, TrainingSet };

pub fn compute_error(p: &Perceptron, training_set: &TrainingSet) -> f64 {
    let inputs: Vec<Vec<f64>> = training_set.inputs.clone();
    let targets: Vec<Vec<f64>> = training_set.targets.clone();
    let predictions: Vec<f64> = inputs.iter().map(|input| p.activate(input)).collect();

    compute_mean_squared_error(&predictions, &targets)
}

fn compute_mean_squared_error(predictions: &[f64], targets: &[Vec<f64>]) -> f64 {
    assert_eq!(predictions.len(), targets.len(), "Prediction and target length mismatch");

    let sum_squared_error: f64 = predictions
        .iter()
        .zip(targets)
        .map(|(&prediction, target)| {
            let target_value = target.iter().position(|&x| x == 1.0).unwrap(); // Find the index of the target value with a 1.0
            (prediction - target_value as f64).powi(2)
        })
        .sum();

    let mean_squared_error = sum_squared_error / predictions.len() as f64;
    mean_squared_error
}