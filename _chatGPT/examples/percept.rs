/**
A simple neural network for linearly separable problems.

This example program generates
- the letters X and O
- as a flattened 5x5 pixel vector of a 1-channel image 
- with pixel intensities in either a perfect form with 1.0 and 0.0 values 
- or in a more blurry fashion, where active pixel intensities range from 0.7 to 1.0 and inactive pixels range from 0.0 to 0.2

However, the most incredible thing about this program is, that is has been mostly programmed by ChatGPT.

The complete conversation of this endavour can be read here:

https://chat.openai.com/share/cc6657b4-8239-4145-90a0-cfcbe45814ae

After a while I was stuck in errors. Search for the term 'start over' to jump to the point where I tried to start over.
The second attempt was much smoother and led to a working example in less than say 2 hours.

Although, ChatGPT makes lots of errors, I have to admit, it's a much better programmer than I am. I would not have been able to code
this small program in such a small time. I have a CS background and know how Artificial Neural Networks work in princinple, but have only 
coded a single ann that [predicts the winner of football matches](https://gitlab.com/aspera-non-spernit/guru) based on previous results (>80% success rate N3rd division NISA soccer league in the US)
and a genetic algorithm to solve the [travelling sales person problem](https://gitlab.com/aspera-non-spernit/tsp).

Initially I wanted to code a unsupervised algorithm, but ChatGPT diverted from my intention and i didn't bother.
**/

use rand::Rng;
use rand::prelude::SliceRandom;
use percept::{ ActivationFunction, error, Layer, Perceptron, Train, TrainingSet };

const X: [usize; 9] = [0, 4, 6, 8, 12, 16, 18, 20, 24];
const O: [usize; 12] = [1, 2, 3, 5, 9, 10, 14, 15, 19, 21, 22, 23];


fn generate_letter(pixels: &[usize], model: bool) -> Vec<f64> {
    let mut model_high = 0.7;
    let mut model_low = 0.2;

    if model == true { model_high = 1.0 ; model_low = 0.0; }

    let mut rng = rand::thread_rng();
    (0..25)
        .map(|i| {
            if  pixels.contains(&i) {
                rng.gen_range(model_high..=1.0)
            } else {
                rng.gen_range(0.0..=model_low)
            }
        }).collect::<Vec<f64>>()
}

fn examples() -> TrainingSet {
    let mut examples = Vec::new();

    for _ in 0..1 {
        examples.push((generate_letter(&X, false), vec![1.0, 0.0]));
        examples.push((generate_letter(&O, false), vec![0.0, 1.0]));
    }

    let mut rng = rand::thread_rng();
    examples.shuffle(&mut rng);
    let (inputs, targets): (Vec<Vec<f64>>, Vec<Vec<f64>>) = examples.into_iter().unzip();
    TrainingSet {
        inputs,
        targets,
    }
}

fn test_recognition(layer: &Layer, test_set: &TrainingSet) -> f64 {
    let mut correct_count = 0;

    for (input, target) in test_set.inputs.iter().zip(&test_set.targets) {
        let predictions: Vec<f64> = layer.perceptrons.iter().map(|p| p.activate(input)).collect();

        println!("Input: {:?}", input);
        println!("Predictions: {:?}", predictions);
        println!("Target: {:?}", target);
        let max_value = predictions.iter().fold(f64::NEG_INFINITY, |max, &x| x.max(max));
        let predicted_index = predictions.iter().position(|&x| (x - max_value).abs() < f64::EPSILON).unwrap();
        
        let max_value = target.iter().fold(f64::NEG_INFINITY, |max, &x| x.max(max));
        let target_index = target.iter().position(|&x| (x - max_value).abs() < f64::EPSILON).unwrap();
        
        if predicted_index == target_index {
            correct_count += 1;
        }

        println!();
    }

    let accuracy = correct_count as f64 / test_set.inputs.len() as f64;
    accuracy
}

fn generate_test_set() -> TrainingSet {
    let mut test_set = TrainingSet {
        inputs: vec![
            generate_letter(&X, true),
            generate_letter(&X, false),
            generate_letter(&O, true),
            generate_letter(&O, false),

        ],
        targets: vec![
            vec![1.0, 0.0], // X target
            vec![1.0, 0.0], // X target
            vec![0.0, 1.0], // O target
            vec![0.0, 1.0], // O target
        ],
    };

    test_set
}


fn main() {
    let training_set = examples();

    let mut layer = Layer::new(
        training_set.targets[0].len(), // Number of inputs based on the size of the first input vector
        training_set.inputs[0].len(),  // Number of perceptrons based on the size of the targets vector
        ActivationFunction::Sigmoid, // Choose the desired activation function
    );
    dbg!(&layer);

    layer.train(&training_set, 0.1, 10000);
    dbg!(&layer);


    let accuracy = test_recognition(&layer, &generate_test_set());
    println!("Recognition accuracy: {:.2}%", accuracy * 100.0);
}