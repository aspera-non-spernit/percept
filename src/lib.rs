#![allow(unused_assignments)]
#![allow(unused_imports)]
#![allow(dead_code)]

pub mod adverserial {
    use rand::{ Rng };

    fn flip_signs(layers: &mut Vec<Vec<f64>>, layer_index: usize, weight_index: Option<usize>) {
        if let Some(layer) = layers.get_mut(layer_index) {
            match weight_index {
                Some(index) => {
                    if let Some(weight) = layer.get_mut(index) {
                        *weight = -(*weight);
                    }
                }
                None => {
                    for weight in layer.iter_mut() {
                        *weight = -(*weight);
                    }
                }
            }
        }
    }

    fn prune(layers: &mut Vec<Vec<f64>>, pruning_percentage: f64) {
        let mut rng = rand::thread_rng();

        for layer in layers.iter_mut() {
            // Calculate the threshold as a percentage of the maximum weight magnitude in the layer
            let threshold = pruning_percentage * layer.iter().cloned().fold(f64::NEG_INFINITY, f64::max).abs();

            // Prune weights below the threshold
            for weight in layer.iter_mut() {
                if rng.gen::<f64>() < threshold {
                    *weight = 0.0;
                }
            }
        }
    }
}


// ChatGPT as co-pilot - 2nd attempt
pub mod activations {
   // pub type ActivationFn = Box<dyn Fn(&f64) -> f64>;
    pub type ActivationFn = fn(f64) -> f64;
    pub fn relu(signal: f64) -> f64 { f64::max(0.0, signal) }
    pub fn sigmoid(signal: f64) -> f64 { 1.0 / (1.0 + (-signal).exp()) }
    pub fn softmax(signal: f64) -> f64 { signal.exp() }
    pub fn tanh(signal: f64) -> f64 { signal.tanh() }

    pub fn activate(func: ActivationFn, signal: f64) -> f64 {
        match func {
            _ if func == relu => relu(signal),
            _ if func == sigmoid => sigmoid(signal),
            _ if func == softmax => softmax(signal),
            _ if func == tanh => tanh(signal),
            _ => {
                signal
            }
        }
    }
}

use rand::{ Rng, seq::SliceRandom };
use crate::{
    activations::{activate, ActivationFn, relu, sigmoid, softmax,tanh},
    generate::{generate_letter, training_set}
};


fn normalize_layer(layer: &mut Vec<f64>) {
    // Calculate the mean
    let mean: f64 = layer.iter().sum::<f64>() / layer.len() as f64;

    // Calculate the standard deviation
    let variance: f64 = layer.iter().map(|&x| (x - mean) * (x - mean)).sum::<f64>() / layer.len() as f64;
    let std_dev: f64 = variance.sqrt();

    // Normalize each element
    for val in layer.iter_mut() {
        *val = (*val - mean) / std_dev;
    }
}

fn mult(signals: &[f64], layer: &[f64]) -> Vec<f64> {
    layer.iter().map(|&w| signals.iter().map(|&s| s * w).sum()).collect()
}

fn calculate_mse(output: &[f64], target: &[f64]) -> f64 {
    output.iter().zip(target.iter()).map(|(o, t)| (o - t).powi(2)).sum::<f64>() / output.len() as f64
}

fn backpropagation(learning_rate: f64, input: &[f64], target_output: &[f64], mut layer_weights: Vec<Vec<f64>>) -> Vec<Vec<f64>> {
    let current_input = input.to_vec();
    let mut error_gradients = input
        .iter()
        .zip(target_output.iter())
        .map(|(o, t)| 2.0 * (o - t))
        .collect::<Vec<f64>>();

    for layer in layer_weights.iter_mut().rev() {
        // Update weights
        for (i, weight) in layer.iter_mut().enumerate() {
            if i < current_input.len() {
                *weight -= learning_rate * error_gradients[i] * current_input[i];
            }
        }

        // Backpropagate error to the previous layer
        let updated_error_gradients = layer
            .iter()
            .map(|&w| error_gradients.iter().map(|&g| g * w).sum())
            .collect::<Vec<f64>>();

        // Update the error_gradients for the next iteration
        error_gradients = updated_error_gradients;
    }
    layer_weights
}

trait Train {
    type C;
    fn train(&mut self, config: &Self::C, input: &[f64], target_output: &[f64]);
}

#[derive(Debug)]
struct Network { layer_weights: Vec<Vec<f64>> }

impl Network {
    fn new(topology: &[usize]) -> Self {
        let layer_weights = Network::generate_layer_weights(topology);
        Self { layer_weights }
    }
    fn generate_layer_weights(topology: &[usize]) -> Vec<Vec<f64>> {
        topology
            .iter()
            .map(|i| {
                Network::rand_weights(*i) 
            })
            .collect()
    }

    fn rand_weights(num: usize) -> Vec<f64> {
        let mut rng = rand::thread_rng();
        (0..num).map(|_| {
            rng.gen::<f64>()
        }).collect()
    }
    fn clip_gradients(&mut self, max_gradient: f64) {
        for layer in &mut self.layer_weights {
            for weight in layer {
                if *weight > max_gradient {
                    *weight = max_gradient;
                } else if *weight < -max_gradient {
                    *weight = -max_gradient;
                }
            }
        }
    }
}

#[derive(Debug)]
struct Config {
    learning_rate: f64,
    threshold: f64,
    max_iterations: usize,
    act_fn: ActivationFn
}

impl Train for Network {
    type C = Config;
    fn train(&mut self, config: &Self::C, input: &[f64], target_output: &[f64]) {
        let mut iteration = 0;
        let mut signals = input.to_vec();
        let mut error = f64::MAX;
        while error > config.threshold && iteration < config.max_iterations {
            for layer in &self.layer_weights {
                signals = mult(&signals, layer);
                signals = signals.iter().map(|&x| activate(config.act_fn, x)).collect(); // Apply activation function
            }
            // Calculate error using 'signals' instead of 'input'
            error = calculate_mse(&signals, &target_output);
            // Backpropagation to update weights
            self.layer_weights = backpropagation(config.learning_rate, &signals, &target_output, self.layer_weights.clone());
            iteration += 1;
        }
    }
}

fn test(input: &[f64], layer_weights: Vec<Vec<f64>>, act_fn: ActivationFn) -> Vec<f64> {
    let mut test_output = input.to_vec();
    for layer in &layer_weights {
        test_output = mult(&test_output, layer);
        test_output = test_output.iter().map(|&x| activate(act_fn, x)).collect(); // Apply activation function
    }
    test_output
}