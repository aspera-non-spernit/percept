use rand::Rng;
use std::convert::TryInto;

const SIZE_X: usize = 9;
const SIZE_Y: usize = 1;

#[derive(Debug)]
struct Signal {
    values: [[f32; 3]; 3]
}

trait ToNoisy {
    fn to_noisy(&self);
}

impl ToNoisy for Signal {
    fn to_noisy(&self) {
        let mut rng = rand::thread_rng();
        let noisy: [[f32; 3]; 3] = self.values
            .iter()
            .map(|row| {
                row.iter()
                    .map(|&value| {
                        if value == 1.0 {
                            rng.gen_range(0.7..1.0)
                        } else {
                            rng.gen_range(0.0..0.3)
                        }
                    })
                    .collect::<Vec<_>>()
                    .try_into()
                    .unwrap()
            })
            .collect::<Vec<_>>()
            .try_into()
            .unwrap();
            
        self.values = noisy;
    }
}

fn weight_matrix() -> [[f32; SIZE_Y]; SIZE_X] { //[[f64; 3]; 3]
    let mut rng = rand::thread_rng();
    
    [[0.0; SIZE_Y]; SIZE_X]
        .iter()
        .map(|row| row
            .iter()
            .map(|_| rng.gen_range(0.0..1.0))
            .collect::<Vec<_>>()
            .try_into()
            .unwrap()
        )
        .collect::<Vec<_>>()
        .try_into()
        .unwrap()
}

fn to_noisy(matrix: &[[f32; 3]; 3]) -> [[f32; 3]; 3] {
    let mut rng = rand::thread_rng();
    matrix
        .iter()
        .map(|row| row
            .iter()
            .map(|&value| {
                if value == 1.0 {
                    rng.gen_range(0.7..1.0)
                } else {
                    rng.gen_range(0.0..0.3)
                }
            })
            .collect::<Vec<_>>()
            .try_into()
            .unwrap()
        )
        .collect::<Vec<_>>()
        .try_into()
        .unwrap()
}

fn multiply_matrices(a: &[[f32; SIZE_X]], b: &[[f32; SIZE_Y]; SIZE_X]) -> [[f32; SIZE_Y]; 1] {
    let mut result = [[0.0; SIZE_Y]; 1];

    for i in 0..SIZE_Y {
        for j in 0..1 {
            for k in 0..SIZE_X {
                result[i][j] += f32::from(a[j][k]) * b[k][i];
            }
        }
    }
    result
}