fn encode_img_format(input: &str) -> Vec<u8> {
    let mut binary_representation = Vec::new();

    for ch in input.chars() {
        let binary = format!("{:b}", ch as u8); // Use 6 bits for each character
       // println!("Character: {}, Binary: {}", ch, binary); // Debugging
        binary_representation.extend(binary.chars().map(|bit| bit.to_digit(2).unwrap() as u8));
    }
    binary_representation
}

fn decode_img_format(input: &[u8]) -> String {
    let mut result = String::new();

    // Iterate over input bytes
    for &byte in input {
        // Convert each byte to a binary string representation with 8 bits
        let binary_str = format!("{:08b}", byte);
        
        // Take every 6 bits and convert them back to a character
        for chunk in binary_str.as_bytes().chunks(6) {
            // Pad the chunk if it's not 6 bits long
            let mut padded_chunk = Vec::from(chunk);
            padded_chunk.resize(6, b'0');

            // Convert the 6-bit chunk back to a character and append it to the result
            let char_value = u8::from_str_radix(std::str::from_utf8(&padded_chunk).unwrap(), 2).unwrap();
            result.push(char::from(char_value));
        }
    }

    result
}

fn encode(img: &Vec<Vec<u8>>) -> Vec<String> {
    img.iter()
        .map(|row| {
            row.iter()
                .map(|&pixel| format!("{:08b}", pixel))
                .collect::<String>()
        })
        .map(|binary_row| {
            binary_row
                .chars()
                .collect::<Vec<_>>()
                .chunks(2)
                .map(|chunk| match chunk {
                    ['0', '0'] => 'A',
                    ['0', '1'] => 'C',
                    ['1', '0'] => 'G',
                    ['1', '1'] => 'T',
                    _ => panic!("Invalid binary string"),
                })
                .collect::<String>()
        })
        .collect()
}

fn decode(dna_encoded_image: &Vec<String>) -> Vec<Vec<u8>> {
    dna_encoded_image
        .iter()
        .map(|row| {
            row.chars()
                .map(|nucleotide| match nucleotide {
                    'A' => "00",
                    'C' => "01",
                    'G' => "10",
                    'T' => "11",
                    _ => panic!("Invalid nucleotide"),
                })
                .collect::<String>()
        })
        .map(|binary_row| {
            binary_row
                .as_bytes()
                .chunks(8)
                .map(|chunk| {
                    u8::from_str_radix(std::str::from_utf8(chunk).unwrap(), 2).unwrap()
                })
                .collect::<Vec<_>>()
        })
        .collect()
}

const options: [&str; 3] =["jpg", "png", "gif"]; // Example options

fn main() {
    let img_format = encode_img_format("png");

    dbg!(&img_format.len());

    let img: Vec<Vec<u8>> = vec![
        img_format,
        vec![0, 127, 128, 255, 63],
        vec![100, 50, 10, 255, 123],
        vec![0, 127, 128, 255, 63],
        vec![30, 80, 150, 200, 34],
        vec![0, 127, 128, 255, 63],
        vec![0, 0, 0, 0, 0],
    ];
   
    let dna_encoded_image: Vec<String> = encode(&img);
    dbg!(&dna_encoded_image);
    

}
